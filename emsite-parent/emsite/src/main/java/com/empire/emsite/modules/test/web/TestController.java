/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.test.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.entity.User;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.empire.emsite.modules.test.entity.Test;
import com.empire.emsite.modules.test.facade.TestFacadeService;
import com.unj.dubbotest.provider.DemoService;

/**
 * 类TestController.java的实现描述：测试Controller
 * 
 * @author arron 2017年10月30日 下午7:24:48
 */
@Controller
@RequestMapping(value = "${adminPath}/test/test")
public class TestController extends BaseController {
    @Autowired
    private DemoService       demoService;
    @Autowired
    private TestFacadeService testFacadeService;

    @ModelAttribute
    public Test get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return testFacadeService.get(id);
        } else {
            return new Test();
        }
    }

    /**
     * 显示列表页
     * 
     * @param test
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequiresPermissions("test:test:view")
    @RequestMapping(value = { "list", "" })
    public String list(Test test, HttpServletRequest request, HttpServletResponse response, Model model) {
        String hello = demoService.sayHello("guoxue");
        System.out.println(hello);

        List list = demoService.getUsers();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
        return "modules/test/testList";
    }

    /**
     * 获取硕正列表数据（JSON）
     * 
     * @param test
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequiresPermissions("test:test:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<Test> listData(Test test, HttpServletRequest request, HttpServletResponse response, Model model) {
        User user = UserUtils.getUser();
        if (!user.isAdmin()) {
            test.setCreateBy(user);
        }
        Page<Test> page = testFacadeService.findPage(new Page<Test>(request, response), test);
        return page;
    }

    /**
     * 新建或修改表单
     * 
     * @param test
     * @param model
     * @return
     */
    @RequiresPermissions("test:test:view")
    @RequestMapping(value = "form")
    public String form(Test test, Model model) {
        model.addAttribute("test", test);
        return "modules/test/testForm";
    }

    /**
     * 表单保存方法
     * 
     * @param test
     * @param model
     * @param redirectAttributes
     * @return
     */
    @RequiresPermissions("test:test:edit")
    @RequestMapping(value = "save")
    public String save(Test test, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, test)) {
            return form(test, model);
        }
        test.setCurrentUser(UserUtils.getUser());
        testFacadeService.save(test);
        addMessage(redirectAttributes, "保存测试'" + test.getName() + "'成功");
        return "redirect:" + adminPath + "/test/test/?repage";
    }

    /**
     * 删除数据方法
     * 
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequiresPermissions("test:test:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(Test test, RedirectAttributes redirectAttributes) {
        testFacadeService.delete(test);
        addMessage(redirectAttributes, "删除测试成功");
        return "redirect:" + adminPath + "/test/test/?repage";
        //		return "true";
    }

}
