/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import com.empire.emsite.modules.sys.entity.Area;

/**
 * 类AreaType.java的实现描述：字段类型转换[ TODO 该类需要重写]
 * 
 * @author arron 2017年10月30日 下午1:08:24
 */
public class AreaType {

    /**
     * 获取对象值（导入）[ TODO 该方法需要重写]
     */
    public static Object getValue(String val) {
        return null;
    }

    /**
     * 获取对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null && ((Area) val).getName() != null) {
            return ((Area) val).getName();
        }
        return "";
    }
}
